#!/usr/bin/env python3

'''
This is very simple demo code based on some simple reverse engineering work.
Its goal is not provide features but just show you how you can communicate with this device.
Expect nothing fancy. 
'''

import hid
import time
from datetime import datetime

'''
Product: ShuttleXpress
Manufacturer: Contour Design
'''

vid=0x0b33
pid=0x0020
length=5

h = hid.device()
h.open(vid, pid)

def write_to_hid(dev, cmd):
  try:
    num_bytes_written = dev.write(cmd)
  except IOError as e:
    print ('Error writing command: {}'.format(e))
    return None 

  return num_bytes_written

def read_from_hid(dev, timeout):
  try:
    data = dev.read(length, timeout)
  except IOError as e:
    print ('Error reading response: {}'.format(e))
    return None
  return data

print(datetime.now());
print(datetime.now());
print(datetime.now(), "Now you can see the HID output from the device  and");
print(datetime.now(), "have some fun by pressing, turning knob etc...");
print(datetime.now());
print(datetime.now(), "Press Ctrl-C to quit");
print(datetime.now());
print(datetime.now(), "You should see one line on every status change");
print(datetime.now());

while True:
  xyz = read_from_hid(h,10)
  if xyz:
    print(datetime.now(), xyz)
