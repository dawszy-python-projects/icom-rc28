#!/usr/bin/env python3

'''
Iambic logic based on: http://m0xpd.blogspot.com/2012/12/keyer-on-rpi.html (M0XPD)
'''

import hid
import time
import serial

vid=0x1fc9
pid=0x0003
length=17

h = hid.device()
h.open(vid, pid)

icomUSB = serial.Serial('/dev/ic9700a', 115200, timeout=None)
icomUSB.setDTR(False);

def read_from_hid(dev, timeout):
  try:
    data = dev.read(length, timeout)
  except IOError as e:
    print ('Error reading response: {}'.format(e))
    return None
  return data

wpm = 18
DitLength = 60/(50*wpm)
DahLength = DitLength*3

last = "none"

def SendDit(DitLength):
 """ Sends a Dit..."""
 KeyLength=DitLength
 KeyDown(KeyLength)
 Last_Element="dit"
 return Last_Element

def SendDah(DahLength):
 """ Sends a Dah..."""
 KeyDown(DahLength)
 Last_Element="dah"
 return Last_Element

def SendSpace(DitLength):
 """ Wait for inter-element space..."""
 time.sleep(DitLength)
 return

def KeyDown(KeyLength):
 """ Keys the TX """
 # set the output to Key Down...
 icomUSB.setDTR(True);
 icomUSB.flush();
 time.sleep(KeyLength)
 # clear the output ...
 icomUSB.setDTR(False);
 icomUSB.flush();
 return

def paddleState():
  key="none"
  rbuffer = read_from_hid(h,3)
  if rbuffer and rbuffer[0] == 255:
    val = rbuffer[3]
    if val == 16: key="right"
    if val == 32: key="left"
    if val == 48: key="both"
    if val == 0: key="none"
  return(key)

while True:

 paddle=paddleState()

 if (paddle == "left"):
  while (paddle == "left"):
   last=SendDit(DitLength)
   SendSpace(DitLength)
   paddle=paddleState()

 elif (paddle == "right"):
  while (paddle == "right"):
   last=SendDah(DahLength)
   SendSpace(DitLength)
   paddle=paddleState()

 elif (paddle == "both"):
  while (paddle == "both"):
   if last == "dit":
    last=SendDah(DahLength)
   elif last == "dah":
    last=SendDit(DitLength)
   else:
    last=SendDah(DahLength)
   SendSpace(DitLength)
   paddle=paddleState()

 else:
  last="none"
