#!/usr/bin/env python3

'''
This code requires testing - it was refactored but not tested. Treat this file as a documentation - do not expect it to run. To be fixed soon.
'''

'''
This is very simple demo code based on some simple reverse engineering work.
Its goal is not provide features but just show you how you can communicate with this device.
Expect nothing fancy. 
'''

import hid
import time
from datetime import datetime

'''
Product: Icom RC-28 REMOTE ENCODER
Manufacturer: Icom Inc.
'''

vid=0x0c26
pid=0x001e
length=8

h = hid.device()
h.open(vid, pid)

def write_to_hid(dev, cmd):
  try:
    num_bytes_written = dev.write(cmd)
  except IOError as e:
    print ('Error writing command: {}'.format(e))
    return None 
  return num_bytes_written

def read_from_hid(dev, timeout):
  try:
    data = dev.read(length, timeout)
  except IOError as e:
    print ('Error reading response: {}'.format(e))
    return None
  return data

print(datetime.now(), "Turn ON all possible LED's");
write_to_hid(h,[0x01,0b0000000]);
time.sleep(2);
print(datetime.now(), "Turn OFF all possible LED's");
write_to_hid(h,[0x01,0b0001111]);
time.sleep(2);
print(datetime.now(), "Turn ON LINK LED");
write_to_hid(h,[0x01,0b0000111]);
time.sleep(2);
print(datetime.now(), "Turn ON F2 LED");
write_to_hid(h,[0x01,0b0001011]);
time.sleep(2);
print(datetime.now(), "Turn ON F1 LED");
write_to_hid(h,[0x01,0b0001101]);
time.sleep(2);
print(datetime.now(), "Turn ON TRANSMIT LED");
write_to_hid(h,[0x01,0b0001110]);
time.sleep(2);
print(datetime.now(), "Turn OFF all possible LED's");
write_to_hid(h,[0x01,0b0001111]);
time.sleep(2);

print(datetime.now(), "Ask for serial number");
write_to_hid(h,[0x02]);
xyz = read_from_hid(h,10)
if xyz:
  print(datetime.now(), xyz)

print(datetime.now(), "Ask for what?");
write_to_hid(h,[0x04]);
xyz = read_from_hid(h,10)
if xyz:
  print(datetime.now(), xyz)

print(datetime.now());
print(datetime.now());
print(datetime.now(), "Now you can see the HID output from the device  and");
print(datetime.now(), "have some fun by pressing, turning knob etc...");
print(datetime.now());
print(datetime.now(), "Press Ctrl-C to quit");
print(datetime.now());
print(datetime.now(), "You should see one line on every status change");
print(datetime.now());

time.sleep(1);

while True:
  xyz = read_from_hid(h,10)
  if xyz:
    print(datetime.now(), xyz)
