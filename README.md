# Intro

This is very simple demo code based on some simple reverse engineering work.
Its goal is not provide features but just show you how you can communicate with those devices.
Expect nothing fancy. 

# Devices with working code

On Linux please do not forget to update your udev with following rules:

## For ICOM RC28:
```
SUBSYSTEM=="usb", ATTRS{idVendor}=="0c26", ATTRS{idProduct}=="001e", MODE="0664", GROUP="plugdev"
```

## For contour ShuttleXpress:
```
SUBSYSTEM=="usb", ATTRS{idVendor}=="0b33", ATTRS{idProduct}=="0020", MODE="0664", GROUP="plugdev"
```

## For EXPERT-ELECT eCoderPlus (SunSDR):
```
SUBSYSTEM=="usb", ATTRS{idVendor}=="1fc9", ATTRS{idProduct}=="0003", MODE="0664", GROUP="plugdev"
```
