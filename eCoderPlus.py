#!/usr/bin/env python3

'''
This is very simple demo code based on some simple reverse engineering work.
Its goal is not provide features but just show you how you can communicate with this device.
Expect nothing fancy. 
'''

import hid
import time
from datetime import datetime

'''
Product: E-Coder panel      
Manufacturer: EXPERT-ELECT 
'''

vid=0x1fc9
pid=0x0003
length=17

h = hid.device()
h.open(vid, pid)

def write_to_hid(dev, cmd):
  try:
    num_bytes_written = dev.write(cmd)
  except IOError as e:
    print ('Error writing command: {}'.format(e))
    return None 

  return num_bytes_written

def read_from_hid(dev, timeout):
  try:
    data = dev.read(length, timeout)
  except IOError as e:
    print ('Error reading response: {}'.format(e))
    return None
  return data

print(datetime.now(), "Turn ON all possible LED's");
write_to_hid(h,[0b0000000]);
time.sleep(2);
print(datetime.now(), "Turn OFF all possible LED's (ver1)");
write_to_hid(h,[0b1100000]);
time.sleep(2);
print(datetime.now(), "Turn ON buttons backlight");
write_to_hid(h,[0b0010000]);
time.sleep(2);
print(datetime.now(), "Turn OFF all possible LED's (ver2)");
write_to_hid(h,[0b0100000]);
time.sleep(2);
print(datetime.now(), "Turn ON green LED1");
write_to_hid(h,[0b0000001]);
time.sleep(2);
print(datetime.now(), "Turn ON red LED2");
write_to_hid(h,[0b0000100]);
time.sleep(2);
print(datetime.now(), "Turn ON yellow LED3");
write_to_hid(h,[0b0000010]);
time.sleep(2);
print(datetime.now(), "Turn ON VFO knob backlight");
write_to_hid(h,[0b0001000]);
time.sleep(2);
print(datetime.now(), "Turn OFF all possible LED's (ver3)");
write_to_hid(h,[0b1000000]); # backlight OFF
time.sleep(2);
print(datetime.now());
print(datetime.now());
print(datetime.now(), "In 10 seconds we will start dumping HID bytes ");
print(datetime.now(), "so you see the HID output from the device  and");
print(datetime.now(), "have some fun by pressing, turning knob etc...");
print(datetime.now());
print(datetime.now(), "Do not forget to test your PTT pedal and ");
print(datetime.now(), "cw paddle or straight key on JACK inputs ");
print(datetime.now());
print(datetime.now(), "Press Ctrl-C to quit");
print(datetime.now());
print(datetime.now(), "You should see one line every ~2ms");
print(datetime.now());

time.sleep(10);

while True:
  xyz = read_from_hid(h,10)
  print(datetime.now(), xyz)
